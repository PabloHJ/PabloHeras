package Pruebas2;
import java.util.Scanner;

public class P1 {
	static Scanner lector = new Scanner(System.in);
	
	public static void main (String [] args) {
		
		//VARIABLES
		int x,y;
		
		//PEDIMOS LOS DATOS AL USUARI
		System.out.println("Dime un numero");
		x = lector.nextInt();
		
		System.out.println("Dime otro numero");
		y = lector.nextInt();
		
		//COMPROBAMOS
		if(x > y) {
			System.out.println( x + " es mayor que " + y );

		}else if (x < y) {
			System.out.println( y + " es mayor que " + x );

		}
		else {
			System.out.println( x + " y " + y + " son iguales");
		
		}
		
	}

}
