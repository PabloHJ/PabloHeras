package Pruebas2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class P5 {

	static Scanner lector = new Scanner(System.in);
	static DecimalFormat dc = new DecimalFormat("#.##");
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//VARIABLES
		double numero;
		
		//PEDIMS UN NUMERO POR TECLADO
		System.out.println("Dime un numero");
		numero = lector.nextDouble();
		
		//CALCULAMOS EL IVA
		double iva = 0.21;
		double total = numero + (numero * iva);
		
		//MOSTRAMOS RESULTADO
		System.out.println("EL CALCULO TOTAL DEL IVA ES " + dc.format(total));
		
		
		
	}

}
