package Pruebas2;

import java.text.DecimalFormat;
import java.util.Scanner;

public class P3 {

	static Scanner lector = new Scanner(System.in);
	static DecimalFormat dc = new DecimalFormat("#.##");
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//VARIABLES
		String radio = " ";
		
		//PEDIMOS EL RADIO AL USUARIO
		System.out.println("Dime el radio");
		radio = lector.nextLine();
		
		//PASAMOS EL RADIO A ENTERO Y CALCULAMOS PI
		double r = Double.parseDouble(radio);
		double pi = Math.PI;
		
		//MOSTRAMOS EL RESULTADO
		double area = (pi*(Math.pow(r, 2)));
		System.err.println("El area de el circulo con radio " + r + " es igual a " + dc.format(area));

	}

}
