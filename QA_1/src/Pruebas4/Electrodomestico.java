package Pruebas4;

import java.util.Arrays;



public class Electrodomestico {
	
	//ATRIBUTOS POR DEFECTO
		private String COLOR_DEFECTO = "blanco";
		private char CONSUMO_DEFECTO = 'F';
		private double PRECIO_DEFECTO = 100.0;
		private double PESO_DEFECTO = 5.0;
			
	
	//ATRIBUTOS 
	private double precio_base; 
	char consumo;
	private String color;
	private double peso;
	
	
	//CONSTRUCTOR POR DEFECTO
	public Electrodomestico() {
		PRECIO_DEFECTO = 100;
		COLOR_DEFECTO = "blanco";
		CONSUMO_DEFECTO = 'F';
		PESO_DEFECTO = 5;		
	}
	
	
	//CONSTRUCTOR CON EL PRECIO Y PESO Y EL RESTO POR DEFECTO
	
	public Electrodomestico(double precio_base, double peso) {
		precio_base = precio_base;
		peso = peso;
		this.consumo = CONSUMO_DEFECTO;
		this.color = COLOR_DEFECTO;
		
		
	}
	
	//Un constructor con todos los atributos	
	public Electrodomestico(double precio_base, String color, char consumo, double peso) {
		precio_base = precio_base;
		peso = peso;
		
		if(color!=COLOR_DEFECTO && color!="gris" && color!="rojo" && color!="azul" && color!="negro") {
			System.err.println("Error");
			this.color = COLOR_DEFECTO;
		}else {
			this.color = color;
		}
		if(consumo >= 'A' && consumo <= 'F') {
			this.consumo = consumo;
		}else {
			System.err.println("Error");
			this.consumo = CONSUMO_DEFECTO;
		}

		
		
	}
	
	//GETTERS Y SETTERS

	
	@Override
	public String toString() {
		return "Electrodomestico [precio_base=" + precio_base + ", consumo=" + consumo + ", peso="
				+ peso + ", colores=]";
	}
	
	
	
	
	
}


