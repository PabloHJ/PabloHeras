package Pruebas4;

public class Series {
	
	private String titulo;
	private int numTemporadas;
	private boolean entregado;
	private String creador;
	private String genero;

	public Series() {
		this.titulo = "";
		this.numTemporadas = 3;
		this.entregado = false;
		this.creador = "";
		this.genero = "";
	}
	
	public Series(String titulo, String creador) {
		this.titulo = titulo;
		this.creador = creador;
		this.numTemporadas = 3;
		this.entregado = false;
		this.genero = "";
	}
	
	public Series(String titulo, int numTemporadas, String creador, String genero) {
		this.titulo = titulo;
		this.numTemporadas = numTemporadas;
		this.creador = creador;
		this.genero = genero;
		this.entregado = false;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNumTemporadas() {
		return numTemporadas;
	}

	public void setNumTemporadas(int numTemporadas) {
		this.numTemporadas = numTemporadas;
	}

	public boolean isEntregado() {
		return entregado;
	}

	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	@Override
	public String toString() {
		return "Series [titulo=" + titulo + ", numTemporadas=" + numTemporadas + ", entregado=" + entregado
				+ ", creador=" + creador + ", genero=" + genero + "]";
	}

	

}
