package Pruebas3;

import java.text.DecimalFormat;

import java.util.Scanner;


public class P1 {

	static Scanner lector = new Scanner(System.in);
	static DecimalFormat df = new DecimalFormat("#.##");
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//VARIABLES
		
		double areaCirculo = 0, areaCuadrado = 0, areaTriangulo = 0;
		int opcion = 0;
		
		
		
		//MENU		
		do {
			System.out.println("===========MENU==========");
			System.out.println("Calcular el area de: ");
			System.out.println("1-Circulo");
			System.out.println("2-Triangulo");
			System.out.println("3-Cuadrado");
			System.out.println("Dime la opcion:");
			opcion = lector.nextInt();
			
			//OPCIONES
			switch (opcion) {
			case 1:
				//Llamamos al metodo y le pasamos la variable
				System.out.println("El area del ciruclo es " + df.format(AreaCirculo(areaCirculo)));
				break;

			case 2:
				//Llamamos al metodo y le pasamos la variable
				System.out.println("El area del triagulo es " + df.format(AreaTriangulo(areaTriangulo)));
				break;
				
			case 3:	
				//Llamamos al metodo y le pasamos la variable
				System.out.println("El area del cuadrado es " + df.format(AreaCuadrado(areaCuadrado)));
				break;
				
			default:
				break;
			}
			
			
		}while(opcion!=0);
		System.out.println("FIN DEL PROGRAMA");
			

	}
	
	
	//METODOS
	
	//METODO AREA
	public static double AreaCirculo(double areaC) {
		
		double radioC;
		System.out.println("Dime el radio del circulo");
		radioC = lector.nextDouble();
		
		areaC = (Math.pow(radioC, 2)*Math.PI);
		return areaC;
		
	}
	
	//METODO TRIANGULO
	public static double AreaTriangulo(double areaT) {
		
		double base, altura;
		System.out.println("Dime la base del trianguolo");
		base = lector.nextDouble();
		
		System.out.println("Dime la altura del triangulo");
		altura = lector.nextDouble();
		
		areaT = (base*altura)/2;
		return areaT;
	}
	
	//METODO CUADRADO
	
	public static double AreaCuadrado(double areaCu) {
		
		double lados;
		System.out.println("Dime el tama�o de los lados del cuadrado");
		lados = lector.nextDouble();
		
		areaCu = lados*lados;
		return areaCu;
		
		
		
	}
	
	
}
