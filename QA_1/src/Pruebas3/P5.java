package Pruebas3;

import java.util.Scanner;

public class P5 {

	static Scanner lector = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		//VARIABLES
		int num;
		System.out.println("Dime un numero");
		num = lector.nextInt();
		
		//CONVERTIMOS LOS DATOS A ENTEROS 
		String numBin = BaseDecimal(num);
		System.out.println("El binario de " + num + " es " + numBin);
		
		
		
		
		
	}
	//METODOS 
	
	public static String BaseDecimal(int num) {
		
		String numeroBinario = " ";
		String numeroDecimal = " ";
		
		for (int i = num; i> 0; i/=2) {
			if(i%2==1) {
				numeroDecimal="1";
			}
			else {
				numeroDecimal = "0";
			}
			numeroBinario = numeroDecimal + " - " +  numeroBinario;
			
		}
		return numeroBinario;
		
	}

}
