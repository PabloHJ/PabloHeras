package Pruebas;

public class P5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//VARIABLES
		int A = 5,B = 10,C = 15,D = 20;
		
		System.out.println("VALORES ");
		System.out.println("A = " + A);
		System.out.println("B = " + B);
		System.out.println("C = " + C);
		System.out.println("D = " + D);
		
		//B tome el valor de A
		System.out.println("B TOMA EL VALOR DE A");
		B = A;
		System.out.println("B = " + B);
		
		//C tome el valor de A
		System.out.println("C TOMA EL VALOR DE A");
		C = A;
		System.out.println("C = " + C);
		
		//A tome el valor de D
		System.out.println("A TOMA EL VALOR DE D");
		A = D;
		System.out.println("A = " + A);
		
		//D tome el valor de B
		System.out.println("D TOMA EL VALOR DE B");
		D = B;
		System.out.println("D = " + D);
		
		
	}

}
