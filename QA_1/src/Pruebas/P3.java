package Pruebas;

public class P3 {
	public static void main (String [] args) {
		
		//VARIABLES
		int x = 10, y = 20;
		double n = 30.10, m = 40.20;
		
		//RESULTADOS
		System.out.println("VALORES ENTEROS");
		System.out.println("X = " + x);
		System.out.println("Y = " + y);
		System.out.println("N = " + n);
		System.out.println("M = " + m);
		
		System.out.println("SUMA DE X y Y");
		System.out.println(x+y);
		
		System.out.println("DIFERENCIA DE X y Y");
		System.out.println(x-y);
		
		System.out.println("PRODUCTO DE X y Y");
		System.out.println(x*y);
		
		System.out.println("COCIENTE DE X y Y");
		System.out.println(x/y);
		
		System.out.println("RESTO DE X y Y");
		System.out.println(x%y);
		
		System.out.println("===================================");
		
		System.out.println("VALORES DECIMAS");
		System.out.println("X = " + x);
		System.out.println("Y = " + y);
		System.out.println("N = " + n);
		System.out.println("M = " + m);
		
		System.out.println("SUMA N y M");
		System.out.println(n+m);
		
		System.out.println("DIFERENCIA DE N y M");
		System.out.println(n-m);
		
		System.out.println("PRODUCTO DE N y M");
		System.out.println(n*m);
		
		System.out.println("COCIENTE DE N y M");
		System.out.println(n/m);
		
		System.out.println("RESTO DE N y M");
		System.out.println(n%m);
		
		System.out.println("===================================");
		System.out.println("EL DOBLE DE CADA VARIABLE");
		System.out.println(x*x);
		System.out.println(y*y);
		System.out.println(n*n);
		System.out.println(m*m);
		
		System.out.println("===================================");
		System.out.println("LA SUMA DE TODAS LAS VARIABLES");
		System.out.println(x+y+n+m);
		
		System.out.println("===================================");
		System.out.println("EL PRODUCTO DE TODAS LAS VARIABLES");
		System.out.println(x*y*n*m);
		

		
		
		
	}

}
